class obl_ppm:
    def __init__(self):
        self.plec = input("(M)ale / (F)emale? ")
        self.x = int(input("Podaj swoja wage: "))
        self.y = int(input("Podaj swoj wzrost: "))
        self.z = int(input("Podaj swoj wiek: "))
        self.wynik_1 = (66.5 + (13.7 * self.x) + (5 * self.y) - (6.8 * self.z))
        self.wynik_2 = (655 + (9.6 * self.x) + (1.85 * self.y) - (4.7 * self.z))
        if self.plec == "M":
            print(self.wynik_1)
            self.tryb = float(input("\n1.0 - Brak aktywnosci fizycznej\n1.2 - Aktywnosc na niskim poziomie\n"
                                    "1.4 - Trening 2x w tygodniu\n1.6 - Trening 3-4x w tygodniu\n"
                                    "1.8 - Trening 5x w tygodniu\n2.0 - Codzienny trening\nOkresl swoj tryb zycia: "))
            if self.tryb == 1.0:
                print(self.wynik_1 * 1)
            elif self.tryb == 1.2:
                print(self.wynik_1 * 1.2)
            elif self.tryb == 1.4:
                print(self.wynik_1 * 1.4)
            elif self.tryb == 1.6:
                print(self.wynik_1 * 1.6)
            elif self.tryb == 1.8:
                print(self.wynik_1 * 1.8)
            elif self.tryb == 2.0:
                print(self.wynik_1 * 2.0)
            else:
                print("Nie ma takiego numeru.")
        elif self.plec == "F":
            print(self.wynik_2)
            self.tryb = float(input("\n1.0 - Brak aktywnosci fizycznej\n1.2 - Aktywnosc na niskim poziomie\n"
                                    "1.4 - Trening 2x w tygodniu\n1.6 - Trening 3-4x w tygodniu\n"
                                    "1.8 - Trening 5x w tygodniu\n2.0 - Codzienny trening\nOkresl swoj tryb zycia: "))
            if self.tryb == 1.0:
                print(self.wynik_2 * 1)
            elif self.tryb == 1.2:
                print(self.wynik_2 * 1.2)
            elif self.tryb == 1.4:
                print(self.wynik_2 * 1.4)
            elif self.tryb == 1.6:
                print(self.wynik_2 * 1.6)
            elif self.tryb == 1.8:
                print(self.wynik_2 * 1.8)
            elif self.tryb == 2.0:
                print(self.wynik_2 * 2.0)
            else:
                print("Nie ma takiego numeru.")
        else:
            print("Nie ma takiej plci.")
